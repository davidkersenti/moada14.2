import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesService {

  invoicesObservable;

  constructor(private af:AngularFire) { }

  addInvoice(invoice){

    this.invoicesObservable.push(invoice);
  }
  getInvoices(){
    this.invoicesObservable = this.af.database.list('/invoices').delay(2000)
    return this.invoicesObservable;
	}
   deleteInvoice(invoice){
    let invoiceKey = invoice.$key;
    this.af.database.object('/invoices/' + invoiceKey).remove();
  }
  updateInvoice(invoice){
    let invoiceKey = invoice.$key;
    let invoiceData = {name:invoice.name, amount:invoice.amount};
    this.af.database.object('/invoices/' + invoiceKey).update(invoiceData);
  }
  

}
